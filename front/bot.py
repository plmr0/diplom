from telebot import TeleBot, types
from dotenv import load_dotenv
from threading import Thread

import soundfile as sf
import logging
import functools
import time
import requests
import io
import redis
import os
import json
import ast


load_dotenv()  # Загружаем переменные из .env

# Логи
logging.basicConfig(level=logging.INFO, filename="bot.log", filemode="a", format="%(asctime)s %(message)s")
logging.Formatter.converter = time.gmtime

# Redis
r = redis.Redis(host=os.getenv('REDIS_HOST'), port=int(os.getenv('REDIS_PORT')), decode_responses=True)

# Telegram
bot = TeleBot(os.getenv('BOT_TOKEN'))


def log():
    def actual_decorator(func):
        @functools.wraps(func)
        def _wrapper(*args, **kwargs):
            try:
                if not (message := args[0]):
                    raise_exception(Exception, 'Не удалось получить сообщение')
                chat_id = message.chat.id
                logging.info(message)
                print(message)
                return func(*args, **kwargs)
            except Exception as e:
                send_message(chat_id=chat_id, text=','.join(e.args))
        return _wrapper
    return actual_decorator


def raise_exception(exception, text):
    logging.error(text)
    raise exception(text)


def send_message(*args, **kwargs):
    text = '{chat_id}: {text}'.format(**kwargs).replace('\n', ' ')
    logging.info(text)
    print(text)
    return bot.send_message(*args, **kwargs)


def handle_audio(message, file):
    bot_msg = send_message(chat_id=message.chat.id, text='_Аудиозапись обрабатывается_', parse_mode='markdown', reply_to_message_id=message.message_id)
    payload = {
        'chat_id': message.chat.id,
        'user_message_id': message.message_id,
        'bot_message_id': bot_msg.message_id,
        'file': file
    }
    r.publish(os.getenv('BROKER_SEND_FILE'), json.dumps(payload))


@bot.message_handler(content_types=['voice'])
@log()
def voice(message: types.Message):
    file_info = bot.get_file(message.voice.file_id)
    response = requests.get('https://api.telegram.org/file/bot{0}/{1}'.format(os.getenv('BOT_TOKEN'), file_info.file_path), stream=True)
    ogg = io.BytesIO(response.content)
    data, sample_rate = sf.read(ogg)
    file = f'{os.getenv("FILE_FOLDER")}/{message.message_id}.wav'
    sf.write(file, data, sample_rate)
    handle_audio(message, file)


@bot.message_handler(content_types=['document', 'audio'])
@log()
def get_file(message):
    if message.content_type == 'document':  # wav
        file_info = bot.get_file(message.document.file_id)
        if not file_info.file_path.endswith('.wav'):
            send_message(chat_id=message.chat.id, text='Файл должен быть в *wav* формате', parse_mode='markdown', reply_to_message_id=message.id)
            return
        downloaded_file = bot.download_file(file_info.file_path)
        file = f'{os.getenv("FILE_FOLDER")}/{message.message_id}.wav'
        with open(file, 'wb') as new_file:
            new_file.write(downloaded_file)
        handle_audio(message, file)


def receive():
    p = r.pubsub()
    p.subscribe(os.getenv('BROKER_RECEIVE_FILE'))
    while True:
        try:
            if message := p.get_message():
                data = ast.literal_eval(message.get('data'))
                print(data)
                text = data.get('text', '')
                file_name = f'Аудиозапись {data["user_message_id"]}.txt'
                with open(file_name, 'w') as f:
                    f.write(text.strip() + '\n')
                bot.delete_message(chat_id=data['chat_id'], message_id=data['bot_message_id'])
                with open(file_name, 'rb') as tmp:
                    obj = io.BytesIO(tmp.read())
                    obj.name = file_name
                    print(bot.send_document(chat_id=data['chat_id'], document=obj, caption=data['text'], reply_to_message_id=data['user_message_id']))
                os.remove(data['file'])
                os.remove(file_name)
            time.sleep(0.1)
        except Exception as e:
            print(e)


if __name__ == "__main__":
    if not os.path.isdir(os.getenv('FILE_FOLDER')):
        os.mkdir(os.getenv('FILE_FOLDER'))

    receiver = Thread(target=receive)
    receiver.start()

    bot.polling(none_stop=True, interval=0)
    logging.info('Бот запущен')
